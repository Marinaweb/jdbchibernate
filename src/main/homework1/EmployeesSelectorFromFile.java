package main.homework1;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marina on 4/5/2021.
 */
public class EmployeesSelectorFromFile extends SQLConnection {


    public List<String> selectFromFile() {
        List<String> queryes = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader("D:\\JDBC&Hibernate homeworks\\jdbchibernate\\src\\main\\homework1\\SQLQueryes.txt"))) {
            String s;

            while ((s = br.readLine()) != null) {
                queryes.add(s);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return queryes;
    }

    public void executeQueryFromFile(ArrayList<String> queryes) {
        for (String query: queryes) {
        }
    }
}
