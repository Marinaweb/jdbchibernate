package main.homework1;

import java.util.Date;

/**
 * Created by Marina on 4/1/2021.
 */
public class Employees {
    private int id;
    private String fistName;
    private String lastName;
    private String phone;
    private String adress;

    public Employees(int id, String fistName, String lastName, String phone, String adress) {
        this.id = id;
        this.fistName = fistName;
        this.lastName = lastName;
        this.phone = phone;
        this.adress = adress;
    }

    public int getId() {
        return id;
    }

    public String getFistName() {
        return fistName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhone() {
        return phone;
    }

    public String getAdress() {
        return adress;
    }
}
