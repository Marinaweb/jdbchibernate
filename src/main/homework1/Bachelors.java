package main.homework1;

import java.util.Date;

/**
 * Created by Marina on 4/3/2021.
 */
public class Bachelors {
    private int id;
    private String fistName;
    private String lastName;
    private String phone;
    private Date birthData;
    private boolean maritalStatus;

    public Bachelors(int id, String fistName, String lastName, String phone, Date birthData, boolean maritalStatus) {
        this.id = id;
        this.fistName = fistName;
        this.lastName = lastName;
        this.phone = phone;
        this.birthData = birthData;
        this.maritalStatus = maritalStatus;
    }

    public int getId() {
        return id;
    }

    public String getFistName() {
        return fistName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhone() {
        return phone;
    }

    public Date isBirthData() {
        return birthData;
    }

    public boolean isMaritalStatus() {
        return maritalStatus;
    }
}
