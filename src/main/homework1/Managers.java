package main.homework1;

import java.util.Date;

/**
 * Created by Marina on 4/3/2021.
 */
public class Managers {
    private int id;
    private String fistName;
    private String lastName;
    private String phone;
    private Date birthData;
    private String position;

    public Managers(int id, String fistName, String lastName, String phone, Date birthData, String position) {
        this.id = id;
        this.fistName = fistName;
        this.lastName = lastName;
        this.phone = phone;
        this.birthData = birthData;
        this.position = position;
    }

    public int getId() {
        return id;
    }

    public String getFistName() {
        return fistName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhone() {
        return phone;
    }

    public Date isBirthData() {
        return birthData;
    }

    public String getPosition() {
        return position;
    }
}
