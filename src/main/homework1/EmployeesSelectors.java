package main.homework1;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marina on 4/1/2021.
 */
public class EmployeesSelectors extends SQLConnection {
    public List<Employees> getAllPhoneAndAdress() {
        Connection connection = null;
        Statement statement = null;
        List<Employees> employees = new ArrayList<>();

        try {
            connection = getConnection();
            statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery("SELECT employees.id_employees, employees.first_name, " +
                    "employees.last_name, employees.phone, personaldata.adress \n" +
                    "FROM employees " +
                    "LEFT OUTER JOIN personaldata ON employees.id_employees = personaldata.id_personaldata");

            while (resultSet.next()) {
                int id = resultSet.getInt(1);
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String phone = resultSet.getString("phone");
                String adress = resultSet.getString("adress");

                Employees employee = new Employees(id, firstName, lastName, phone, adress);
                employees.add(employee);

                System.out.println(id + " " + firstName + " " + lastName + " " + phone + " " + adress);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

        return employees;
    }

    public List<Bachelors> getAllBechelorsBirthdayAndPhone() {
        Connection connection = null;
        Statement statement = null;
        List<Bachelors> bachelors = new ArrayList<>();

        try {
            connection = getConnection();
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT employees.id_employees, employees.first_name," +
                    "employees.last_name, employees.phone, personaldata.birth_data, personaldata.marital_status \n" +
                    "FROM employees RIGHT OUTER JOIN personaldata " +
                    "ON employees.id_employees = personaldata.id_personaldata\n" +
                    "WHERE personaldata.marital_status = 0");

            while (resultSet.next()) {
                int id = resultSet.getInt(1);
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String phone = resultSet.getString("phone");
                Date birthData = resultSet.getDate("birth_data");
                Boolean maritalStatus = resultSet.getBoolean("marital_status");

                Bachelors bachelor = new Bachelors(id, firstName, lastName, phone, birthData, maritalStatus);
                bachelors.add(bachelor);

                System.out.println(id + " " + firstName + " " + lastName + " " + maritalStatus
                        + " " + birthData + " " + phone);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
                statement.close();
            } catch (SQLException e) {

            }
        }

        return bachelors;
    }

    public List<Managers> getManagersBirthdataAndPhone() {
        Connection connection = null;
        Statement statement = null;
        List<Managers> managers = new ArrayList<>();

        try {
            connection = getConnection();
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT employees.id_employees, employees.first_name," +
                    "employees.last_name, employees.phone, positions.position, personaldata.birth_data\n" +
                    "FROM employees JOIN positions ON employees.id_employees = positions.id_positions \n" +
                    "JOIN personaldata ON employees.id_employees = personaldata.id_personaldata " +
                    "WHERE position = \"manager\"");

            while (resultSet.next()) {
                int id = resultSet.getInt(1);
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String position = resultSet.getString("position");
                String phone = resultSet.getString("phone");
                Date birthData = resultSet.getDate("birth_data");

                Managers manager = new Managers(id, firstName, lastName, phone, birthData, position);
                managers.add(manager);

                System.out.println(id + " " + firstName + " " + lastName + " " + position
                        + " " + birthData + " " + phone);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
                statement.close();
            } catch (SQLException e) {

            }
        }

        return managers;

    }
}
