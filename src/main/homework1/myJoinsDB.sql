CREATE DATABASE if not exists myJoinsDB;

USE myJoinsDB;

CREATE TABLE IF NOT EXISTS `myJoinsDB`.`employees`
(
  `id_employees` INT         NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(30) NOT NULL,
  `last_name` VARCHAR(40) NOT NULL,
  `phone` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id_employees`)
)
  ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `myJoinsDB`.`positions`
(
  `id_positions`   INT         NOT NULL,
  `position` VARCHAR(40) NOT NULL,
  `salary` DOUBLE NOT NULL,
  `id_employees`   INT         NOT NULL,
  PRIMARY KEY (`id_positions`),
  UNIQUE INDEX `id_positions_UNIQUE` (`id_positions` ASC),
  INDEX `employees_idx` (`id_employees` ASC),
  CONSTRAINT `positions_ibfk_1`
  FOREIGN KEY (`id_employees`)
  REFERENCES `myJoinsDB`.`employees` (`id_employees`)
    ON DELETE CASCADE
    ON UPDATE RESTRICT
)
  ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `myJoinsDB`.`personaldata`
(
  `id_personaldata`   INT         NOT NULL,
  `marital_status` TINYINT(4) NOT NULL,
  `birth_data` DATETIME NOT NULL,
  `adress` VARCHAR(40) NOT NULL,
  `id_personaldata`   INT         NOT NULL,
  PRIMARY KEY (`id_personaldata`),
  UNIQUE INDEX `id_personaldata_UNIQUE` (`id_personaldata` ASC),
  INDEX `employees_idx` (`id_employees` ASC),
  CONSTRAINT `personaldata_ibfk_1`
  FOREIGN KEY (`id_employees`)
  REFERENCES `myJoinsDB`.`employees` (`id_employees`)
    ON DELETE CASCADE
    ON UPDATE RESTRICT
)
  ENGINE = InnoDB;