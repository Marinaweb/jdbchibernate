package main.homework1;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Marina on 4/1/2021.
 */
public class Main extends SQLConnection {
    private static final String SEPARATOR_LINE = "= = = = = = = = = = = = = = = = = = = = = = = = = =";

    public static void main(String[] args) {
        registerDriver();

        Connection connection = null;
        Statement statement = null;
        EmployeesSelectors employeesSelector = new EmployeesSelectors();
        EmployeesSelectorFromFile employeesSelectorFromFile = new EmployeesSelectorFromFile();

        try {
            connection = getConnection();
            statement = connection.createStatement();

            employeesSelectorFromFile.selectFromFile();

            System.out.println("All employees");
            employeesSelector.getAllPhoneAndAdress();
            System.out.println(SEPARATOR_LINE + "\n");

            System.out.println("All bachelors");
            employeesSelector.getAllBechelorsBirthdayAndPhone();
            System.out.println(SEPARATOR_LINE + "\n");

            System.out.println("All managers");
            employeesSelector.getManagersBirthdataAndPhone();
            System.out.println(SEPARATOR_LINE + "\n");

            statement.executeBatch();


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    protected static void registerDriver() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Driver loading success!");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
