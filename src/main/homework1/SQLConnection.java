package main.homework1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Marina on 4/2/2021.
 */
public abstract class SQLConnection {
    public static Connection getConnection() {
        Connection connection = null;

        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/myJoinsDB?serverTimezone=UTC",
                    "root", "rootROOT2020");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return connection;
    }

}
