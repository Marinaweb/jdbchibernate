package pets;

import java.util.List;

/**
 * Created by Marina on 4/7/2021.
 */
public class Main {
    public static void main(String[] args) {
        AnimalHelper ah = new AnimalHelper();

        System.out.println("Find animal by id:");
        Animal animal = ah.getById(2);
        System.out.println(animal.toString());

        System.out.println("Show all animals");
        List<Animal> animals = ah.getAllAnimal();
        printAllAnimals(animals);

        System.out.println("Update animal's data");
        animal.setName("Nusianna");
        animal.setAge(10);
        animal.setTails(false);
        printAllAnimals(animals);

        System.out.println("Add new animal");
        Animal newAnimal = new Animal();
        newAnimal.setName("Som");
        newAnimal.setAge(8);
        newAnimal.setTails(true);
        ah.addAnimal(newAnimal);
        printAllAnimals(animals);

        System.out.println("Remove animal");
        ah.remove(newAnimal);
        printAllAnimals(animals);


    }

    public static void printAllAnimals(List<Animal> animals) {
        for (Animal animal : animals) {
            System.out.println(animal.getId() + " " + animal.getName() + " " + animal.getAge() + " " + animal.isTails());
        }
    }
}
