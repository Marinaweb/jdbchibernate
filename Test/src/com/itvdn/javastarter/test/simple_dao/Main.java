package com.itvdn.javastarter.test.simple_dao;

import com.itvdn.javastarter.test.simple_dao.dao.CarDAO;
import com.itvdn.javastarter.test.simple_dao.dao.ClientDAO;
import com.itvdn.javastarter.test.simple_dao.dao.DAOFactory;
import com.itvdn.javastarter.test.simple_dao.dao.IDAOFactory;
import com.itvdn.javastarter.test.simple_dao.entity.Car;
import com.itvdn.javastarter.test.simple_dao.entity.Client;

import java.util.List;

/**
 * Created by Asus on 31.01.2018.
 */
public class Main {

    public static void main(String[] args) {
        IDAOFactory factory = DAOFactory.getInstance();
        CarDAO carDAO = factory.getCarDAO();
        ClientDAO clientDAO = factory.getClientDAO();

        IDAOFactory daoFactory = DAOFactory.getInstance();
        ClientDAO clientDao = daoFactory.getClientDAO();
        List<Client> clients = clientDao.getAll();

        System.out.println("CLIENTS:");
        for (Client client : clients) {
            System.out.println("id " + client.getId() + " Name " + client.getName() +
                    " Age " + client.getAge() + " Phone " + client.getPhone());
        }

        System.out.println("= = = = = = = = = = = = = ");
        Client client = clientDAO.getById(1);
        System.out.println(client);

        client.setAge(2);
        client.setName("Sergey");
        client.setPhone("67765655445");

        Client newClient = new Client(7, "Katya", 36, "+380663332211");
        ClientDAO clientDAO1 = daoFactory.getClientDAO();
        clientDAO1.insert(newClient);

        ClientDAO clientDAO2 = daoFactory.getClientDAO();
        clientDAO2.delete(newClient);


        System.out.println("CARS: ");
        Car car = carDAO.getById(3);

        System.out.println(car.getId() + " " + car.getMark() + " " + car.getModel() + " " + car.getPrice());

        carDAO.updatePrice(50000, car);

    }

}
