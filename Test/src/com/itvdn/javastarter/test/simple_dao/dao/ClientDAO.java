package com.itvdn.javastarter.test.simple_dao.dao;

import com.itvdn.javastarter.test.simple_dao.entity.Client;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Asus on 31.01.2018.
 */
public interface ClientDAO {

    void insert(Client client);

    void delete(Client client);

    Client getById(int id);

    List<Client> getAll();
}
