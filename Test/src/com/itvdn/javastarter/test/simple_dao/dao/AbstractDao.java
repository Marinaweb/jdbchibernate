package com.itvdn.javastarter.test.simple_dao.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class AbstractDao  {

    public Connection getConnection() {
        Connection connection = null;

        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/carsshop?serverTimezone=UTC",
                    "root", "rootROOT2020");
        } catch(SQLException e) {
            e.printStackTrace();
        }

        return connection;
    }

}


